import React from 'react';

import {AuthBackground} from '../components/authBackground';
import {AuthPageCard} from '../components/authPageCard';

import './signUp.css'

export let Signup = () => {
    return (
        <div className={"signUpPageContainer"}>
            <AuthBackground/>
            <AuthPageCard/>
        </div>
    )
}