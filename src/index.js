import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {Signup} from '../src/pages/signUp'

ReactDOM.render(
  <React.StrictMode>
    <Signup/>
  </React.StrictMode>,
  document.getElementById('root')
);

