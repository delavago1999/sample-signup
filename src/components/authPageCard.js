import React from 'react';

import './authPageCard.css';

import {TextInputField} from './textInputField';
import {Button} from './button' ;

export let AuthPageCard = () => {
    return (
        <div className={"authPageCardContainer"}>
            <div className={"authPageCard"}>
                <Header/>
                <div className={"authPageFormContainer"}>
                    <TextInputField
                        placeholderText="Name"
                    />
                    <TextInputField
                        placeholderText="Email"
                    />
                    <TextInputField
                        placeholderText="Password"
                    />
                    <Button buttonText="Get Started"/>
                </div>
                
            </div>
        </div>
    )
}

let Header = () => {
    return (
        <div className={"authCardHeaderContainer"}>
            <p className={"authCardHeaderText"}>SIGN UP</p>
            <div className={"authCardHeaderAccent"}/>
        </div>
    )
}