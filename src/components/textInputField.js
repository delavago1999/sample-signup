import React from 'react';

import './textInputField.css'

export let TextInputField = (props) => {
    return (
        <div className={"textInputFieldContainer"}>
            <input className={"textInputFieldInputField"} placeholder={props.placeholderText}/>
            <div className={"textInputFieldIconContainer"}>
                <div className={"textInputFieldIcon"}/>
            </div>
        </div>
    )
}