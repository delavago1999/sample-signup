import React from 'react';

import './authBackground.css';

export let AuthBackground = () => {
    return (
        <div className={"authBackgroundContainer"}>
            <div id={"authBackgroundLeftContainer"}/>
            <div id={"authBackgroundRightContainer"}/>
        </div>
    )
}