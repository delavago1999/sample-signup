import React from 'react';

import './button.css';

export let Button = (props) => {
    return (
        <div className={"buttonContainer"}>
            <p className={"buttonText"}>{props.buttonText}</p>
        </div>
    )
}